# Gitlab Skeleton NPM Package

> A simple NPM package that uses semantic release to deploy our package to AND Digitals Gitlab package registry

## The problem

The problem this package is trying to solve is by giving developers at AND Digital the ability to share code across projects while keeping those packages privately scoped under the Gitlab registry.

## What is Semantic Release?

Semantic release is an automation tool that removes all the hassle when it comes to releasing your work into the main branch. It calculates the correct version number of the app, creates release notes which groups commits for that release, notifies when a release has gone out and it can publish your release to NPM. 

The benefits to this approach is that it removes the laborious tasks that have to be done when working on a production piece of code. Think about this, you make a merge into master/main, manually write a release not with all the commits and changes, tell everyone on slack when it got released and then manually make a commit on your branch before merging to update the version (which can clash if there are a lot of contributors). It could take time and that is what semantic release is trying to solve so we can focus our time on better things.

## Git Workflow

### A developer wants to add a new feature to the repo

1. Make a new branch `feature/name`
2. Do your thing 👩🏻‍💻🧑🏻‍💻
3. Make a merge request - follow the conventional commit standard in the MR title & squash the commits (select the option when making a merge request)
4. Merge into the main branch
5. Semantic release will deploy a new version based on the merge request title (if you squash)


## Getting started in making your own NPM package

### 1. Clone this repo 

You want to clone this repo and then change the remote origin to point towards your repo. Once that is setup, just push to your main branch and it should trigger a pipeline build (which will fail).

> Make sure you rename the npm package name before deploying this.

### 2. Get a Gitlab token

If you click on your avatar in Gitlab and then click "Preferences" you should see a screen with color themes on and tha sidebar on the left. On the left sidebar there is a section called "Access Tokens" which will direct you to a new page. Create an access token and enable it to have "api" scope. Once selected, create the token and store that token somewhere locally for now.


### 3. Update the ENV variables in your repo

On the left sidebar of the repo you wish to deploy, there should be an option called settings. Once hovering over that area there should be an option named "CI/CD". Click that and then expand the variables section. You want to now add your variable here, make sure you name it GITLAB_TOKEN.

> You need maintainer settings to be able to do this. Ask one of the principal engineers to make you a maintainer of the repo you wish to work on.

### 4. Make a new commit

Now the env variable is set we just need re-run the failed pipeline. The pipelines should pass and we should be able to see your npm package [here](https://gitlab.com/groups/ANDigital/-/packages). If it does not pass, check the commit message. If it does not follow [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) it will not work.


## Installtion - Let's get this party started!

Spin up a new npm init in another folder, let's call it "test-project" and then run the following command in the terminal:

```
echo @ANDigital:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

> This may vary on windows but who uses windows? ;) 

Now that is all set, let's install our NPM package:

```
npm i @ANDigital/my-npm-package-name
```



